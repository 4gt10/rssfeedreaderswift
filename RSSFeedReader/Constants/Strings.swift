//
//  Strings.swift
//  RSSReader
//
//  Created by 4gt10 on 20.01.16.
//  Copyright © 2016 Artur Chernov. All rights reserved.
//

import Foundation

let UnableToParseRSSFeedByURL = "UnableToParseRSSFeedByURL"
let CheckTheSourceAndTryAgain = "CheckTheSourceAndTryAgain."
let NoDataToParseByURL = "NoDataToParseByURL"
let Search = "Search"
let EnterRSSURL = "EnterRSSURL"
let SourceURL = "SourceURL"
let Load = "Load"
let IncorrectURL = "IncorrectURL"
let Cancel = "Cancel"
let Error = "Error"
let OK = "OK"
let ProbablyInternetConnectionIsLost = "ProbablyInternetConnectionIsLost"