//
//  DateUtils.swift
//  RSSReader
//
//  Created by 4gt10 on 20.01.16.
//  Copyright © 2016 Artur Chernov. All rights reserved.
//

import Foundation

// MARK: - Comparable

public func ==(lhs: NSDate, rhs: NSDate) -> Bool {
    return lhs === rhs || lhs.compare(rhs) == .OrderedSame
}

public func <(lhs: NSDate, rhs: NSDate) -> Bool {
    return lhs.compare(rhs) == .OrderedAscending
}

public func >(lhs: NSDate, rhs: NSDate) -> Bool {
    return lhs.compare(rhs) == .OrderedDescending
}

// MARK: - RSS date format handling

extension NSDate {
    
    class func formatter(format format: String) -> NSDateFormatter {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = format
        return dateFormatter
    }
    
    class func dateFromRSSString(string: String) -> NSDate {
        return NSDate.formatter(format: "EEE, d MMM yyyy HH:mm:ss ZZZ").dateFromString(string) ?? NSDate()
    }
    
    func toRSSString() -> String {
        return self.toString(format: "EEE, d MMM yyyy HH:mm:ss ZZZ")
    }
    
    func toString(format format: String) -> String {
        return NSDate.formatter(format: format).stringFromDate(self)
    }
}