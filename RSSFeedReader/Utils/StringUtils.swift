//
//  StringUtils.swift
//
//
//  Created by 4gt10 on 16.06.15.
//  Copyright (c) 2015 AriyaHouseIT. All rights reserved.
//

import Foundation

func L10N(stringKey: String) -> String {
    
    let StringsTableName = "Strings"
    let UnknownString = "???"
    
    let string = NSBundle.mainBundle().localizedStringForKey(
        stringKey,
        value: UnknownString,
        table: StringsTableName
    )
    
    return string
}
