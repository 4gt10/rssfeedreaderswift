//
//  RSSFeedViewController.swift
//  RSSReader
//
//  Created by 4gt10 on 19.01.16.
//  Copyright © 2016 Artur Chernov. All rights reserved.
//

import UIKit

class RSSFeedViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate {

    @IBOutlet weak var rssTableView: UITableView!
    @IBOutlet weak var loadingAIW: UIActivityIndicatorView!
    
    private var feed = RSSFeed()
    private var monthNames: [String] = []
    
    // MARK: - View controller lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Title
        let changeSourceItem = UIBarButtonItem(barButtonSystemItem: .Search, target: self, action: "changeSource")
        let openRSSURLItem = UIBarButtonItem(barButtonSystemItem: .Action, target: self, action: "openRSSURL")
        let rightBarButtonItems = [openRSSURLItem, changeSourceItem]
        self.navigationItem.rightBarButtonItems = rightBarButtonItems
        
        // Refreshing
        self.configureRefreshControl()
        
        // Table view cells
        self.rssTableView.rowHeight = UITableViewAutomaticDimension;
        self.rssTableView.estimatedRowHeight = 96;  // IB value
        
        self.loadLastViewedRSS()
    }
    
    // MARK: - Refresh control
    
    private let refreshControl = UIRefreshControl()
    
    private func configureRefreshControl() {
        self.refreshControl.tintColor = UIColor.lightGrayColor()
        self.refreshControl.addTarget(self, action: "refresh:", forControlEvents: .ValueChanged)
        self.rssTableView.addSubview(self.refreshControl)
    }
    
    func refresh(sender: UIRefreshControl) {
        sender.endRefreshing()
        self.fetchFeed(url: NSURL(string: self.feed.rssLink)!)
    }
    
    // MARK: - Table view
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return self.monthNames.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let monthName = self.monthNames[section]
        let feedItems = self.getFeedByMonthName(monthName)
        return feedItems.count
    }
    
    func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return self.monthNames[section]
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(RSSFeedCell.cellIdentifier()) as! RSSFeedCell
        let feedItem = self.feed.items[indexPath.row]
        
        cell.feedItemTitleLabel.text = feedItem.title
        cell.feedItemDescriptionLabel.text = feedItem.itemDescription
        cell.feedItemDateLabel.text = feedItem.pubDate.toString(format: "dd/MM/yyyy, HH:mm")
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let feedItem = self.feed.items[indexPath.row]
        if let url = NSURL(string: feedItem.link) {
            UIApplication.sharedApplication().openURL(url)
        }
    }
    
    // MARK: - Alert view
    
    enum AlertButtonIndex: Int {
        case Cancel = 0
        case OK = 1
    }
    
    func alertView(alertView: UIAlertView, clickedButtonAtIndex buttonIndex: Int) {
        if let index = AlertButtonIndex(rawValue: buttonIndex) {
            switch index {
            case .OK:
                let textField = alertView.textFieldAtIndex(0)!
                if let url = NSURL(string: textField.text!) {
                    self.fetchFeed(url: url)
                } else {
                    self.showErrorAlertWithMessage(L10N(IncorrectURL))
                }
            default: break
            }
        }
    }
    
    // MARK: - Actions
    
    func changeSource() {
        if #available(iOS 8.0, *) {
            let alert = UIAlertController(title: L10N(Search), message: L10N(EnterRSSURL), preferredStyle: .Alert)
            alert.addTextFieldWithConfigurationHandler() { textField in
                textField.placeholder = L10N(SourceURL)
                textField.clearButtonMode = .WhileEditing
                textField.text = self.feed.rssLink
            }
            alert.addAction(UIAlertAction(title: L10N(Load), style: .Default) { _ in
                    if let url = NSURL(string: alert.textFields!.first!.text!) {
                        self.fetchFeed(url: url)
                    } else {
                        self.showErrorAlertWithMessage(L10N(IncorrectURL))
                    }
                }
            )
            alert.addAction(UIAlertAction(title: L10N(Cancel), style: .Cancel, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            let alert = UIAlertView(
                title: L10N(Search),
                message: L10N(EnterRSSURL),
                delegate: self,
                cancelButtonTitle: L10N(Cancel),
                otherButtonTitles: L10N(Load)
            )
            alert.alertViewStyle = .PlainTextInput
            let textField = alert.textFieldAtIndex(0)!
            textField.placeholder = L10N(SourceURL)
            textField.clearButtonMode = .WhileEditing
            textField.text = self.feed.rssLink
            alert.show()
        }
    }
    
    func openRSSURL() {
        if let url = NSURL(string: self.feed.link) {
            UIApplication.sharedApplication().openURL(url)
        }
    }
    
    // MARK: - Helpers
    
    // MARK: - Fetching/saving/loading data
    
    private func fetchFeed(url url: NSURL) {
        self.startLoadingAnimations()
        
        let parser = RSSFetcher()
        parser.feedByURL(
            url,
            completion: { feed, error in
                dispatch_async(dispatch_get_main_queue()) {
                    self.stopLoadingAnimations()
                    if let error = error {
                        self.showErrorAlertWithMessage(error.localizedDescription)
                    } else {
                        self.feed = feed!
                        self.sortFeedByDate()
                        self.monthNames = self.getMonthsNamesFromFeed()
                        
                        // Cache
                        self.saveLastViewedRSSURL()
                        
                        // Update UI
                        self.title = self.feed.title
                        self.rssTableView.reloadData()
                    }
                }
            }
        )
    }
    
    struct UserDefaultsConstants {
        static let Defaults = NSUserDefaults.standardUserDefaults()
        static let LastViewedRSSURLKey = "Last viewed RSS URL"
        static let DefaultRSSURLKey = "Default RSS URL"
    }
    
    private func saveLastViewedRSSURL() {
        let url = NSURL(string: self.feed.rssLink)
        UserDefaultsConstants.Defaults.setURL(url, forKey: UserDefaultsConstants.LastViewedRSSURLKey)
    }
    
    private func loadLastViewedRSS() {
        if let lastViewedRSSURL = UserDefaultsConstants.Defaults.URLForKey(UserDefaultsConstants.LastViewedRSSURLKey) {
            self.fetchFeed(url: lastViewedRSSURL)
        } else {
            let defaultURL = NSURL(string: NSBundle.mainBundle().objectForInfoDictionaryKey(UserDefaultsConstants.DefaultRSSURLKey) as! String)
            self.fetchFeed(url: defaultURL!)
        }
    }
    
    // MARK: - UI
    
    private func startLoadingAnimations() {
        self.rssTableView.hidden = true
        self.loadingAIW.startAnimating()
        UIApplication.sharedApplication().networkActivityIndicatorVisible = true
    }
    
    private func stopLoadingAnimations() {
        self.rssTableView.hidden = false
        self.loadingAIW.stopAnimating()
        UIApplication.sharedApplication().networkActivityIndicatorVisible = false
    }
    
    private func showErrorAlertWithMessage(message: String) {
        if #available(iOS 8.0, *) {
            let alert = UIAlertController(title: L10N(Error), message: message, preferredStyle: .Alert)
            alert.addAction(UIAlertAction(title: L10N(OK), style: .Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        } else {
            let alert = UIAlertView(
                title: L10N(Error),
                message: message,
                delegate: nil,
                cancelButtonTitle: L10N(OK)
            )
            alert.show()
        }
        
    }
    
    // MARK: - Sorting/grouping
    
    private func sortFeedByDate() {
        self.feed.items = self.feed.items.sort() { item1, item2 in
            return item1.pubDate > item2.pubDate
        }
    }
    
    private func getMonthsNamesFromFeed() -> [String] {
        var monthsNames: [String] = []
        for feeditem in self.feed.items {
            let monthName = NSDate.formatter(format: "MMMM").stringFromDate(feeditem.pubDate)
            if !monthsNames.contains(monthName) {
                monthsNames.append(monthName)
            }
        }
        return monthsNames
    }
    
    private func getFeedByMonthName(monthName: String) -> [RSSFeedItem] {
        var feedItems: [RSSFeedItem] = []
        for feedItem in self.feed.items {
            let currentMonthName = NSDate.formatter(format: "MMMM").stringFromDate(feedItem.pubDate)
            if (currentMonthName == monthName) {
                feedItems.append(feedItem)
            }
        }
        return feedItems
    }
}
