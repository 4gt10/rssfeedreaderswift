//
//  RSSFeedItem.swift
//  RSSReader
//
//  Created by 4gt10 on 19.01.16.
//  Copyright © 2016 Artur Chernov. All rights reserved.
//

import Foundation

class RSSFeedItem: NSObject {
    
    var link = ""
    var title = ""
    var itemDescription = ""
    var pubDateString = ""
    
    // Utils
    var pubDate: NSDate {
        get {
            return NSDate.dateFromRSSString(self.pubDateString)
        }
        set {
            self.pubDateString = newValue.toRSSString()
        }
    }
}