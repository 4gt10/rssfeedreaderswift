//
//  RSSFeed.swift
//  RSSReader
//
//  Created by 4gt10 on 19.01.16.
//  Copyright © 2016 Artur Chernov. All rights reserved.
//

import Foundation

class RSSFeed: NSObject {
    
    var title = ""
    var link = ""
    var rssLink = ""
    var items: [RSSFeedItem] = []
}