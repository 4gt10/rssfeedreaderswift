//
//  RSSParser.swift
//  RSSReader
//
//  Created by 4gt10 on 19.01.16.
//  Copyright © 2016 Artur Chernov. All rights reserved.
//

import Foundation

class RSSFetcher: NSObject, NSXMLParserDelegate {
    
    private var parser: NSXMLParser?
    private var currentElementName = ""
    private var currentFeed: RSSFeed?
    private var currentItem: RSSFeedItem?
    private var feed = RSSFeed()
    private var completion: ((feed: RSSFeed?, error: NSError?) -> Void)?
    
    // MARK: - Interface
    
    func feedByURL(url: NSURL, completion: (feed: RSSFeed?, error: NSError?) -> Void) {
        self.feed.rssLink = url.absoluteString
        self.completion = completion
        
        if (NetworkUtils.isConnectedToNetwork()) {
            // Online
            let request = NSURLRequest(URL: url)
            let session = NSURLSession(configuration: NSURLSessionConfiguration.defaultSessionConfiguration())
            let task = session.dataTaskWithRequest(
                request,
                completionHandler: { data, response, error in
                    if let error = error  {
                        self.completion?(feed: nil, error: error)
                    } else {
                        if let data = data {
                            self.saveFeedToFile(data)
                            self.parseXMLData(data)
                        } else {
                            let error = self.errorWithHost(
                                url.host,
                                message: L10N(NoDataToParseByURL) + ":/n" + url.absoluteString + "/n" + L10N(CheckTheSourceAndTryAgain)
                            )
                            self.completion?(feed: nil, error: error)
                        }
                    }
                }
            )
            task.resume()
        } else {
            // Offline
            if let data = self.loadFeedFromFile() {
                self.parseXMLData(data)
            } else {
                let error = self.errorWithHost(
                    url.host,
                    message: L10N(ProbablyInternetConnectionIsLost)
                )
                self.completion?(feed: nil, error: error)
            }
        }
    }
    
    // MARK: - NSXMLParserDelegate
    
    enum XMLElement: String {
        case Channel = "channel"
        case Item = "item"
        case Title = "title"
        case Link = "link"
        case Description = "description"
        case PublicationDate = "pubDate"
    }
    
    func parser(parser: NSXMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        self.currentElementName = elementName
        if let element = XMLElement(rawValue: elementName) {
            switch element {
            case .Channel:
                self.currentFeed = RSSFeed()
            case .Item:
                self.currentItem = RSSFeedItem()
            default: break
            }
        }
    }
    
    func parser(parser: NSXMLParser, foundCharacters string: String) {
        let stringWithoutWhitespaceAndNewlineCharacters =
            string.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceAndNewlineCharacterSet())
        // We are inside "item" element
        if let currentItem = self.currentItem {
            if let element = XMLElement(rawValue: self.currentElementName) {
                switch element {
                case .Title:
                    currentItem.title += stringWithoutWhitespaceAndNewlineCharacters
                case .Description:
                    currentItem.itemDescription += stringWithoutWhitespaceAndNewlineCharacters
                case .Link:
                    currentItem.link += stringWithoutWhitespaceAndNewlineCharacters
                case .PublicationDate:
                    currentItem.pubDateString += stringWithoutWhitespaceAndNewlineCharacters
                default: break
                }
            }
        // We are inside "channel" element
        } else if let currentFeed = self.currentFeed {
            if let element = XMLElement(rawValue: self.currentElementName) {
                switch element {
                case .Title:
                    currentFeed.title += stringWithoutWhitespaceAndNewlineCharacters
                case .Link:
                    currentFeed.link += stringWithoutWhitespaceAndNewlineCharacters
                default: break
                }
            }
        }
    }
    
    func parser(parser: NSXMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        self.currentElementName = ""
        if let element = XMLElement(rawValue: elementName) {
            switch element {
            case .Item:
                if let currentItem = self.currentItem {
                    self.feed.items.append(currentItem)
                    self.currentItem = nil
                }
            case .Title, .Link:
                // Avoiding reading attributes in an inner channel entity with the same names
                if let currentFeed = self.currentFeed {
                    if (!currentFeed.title.isEmpty && !currentFeed.link.isEmpty) {
                        self.feed.title = currentFeed.title
                        self.feed.link = currentFeed.link
                        self.currentFeed = nil
                    }
                }
            default: break
            }
        }
    }
    
    func parserDidEndDocument(parser: NSXMLParser) {
        self.completion?(feed: self.feed, error: nil)
    }
    
    func parser(parser: NSXMLParser, parseErrorOccurred parseError: NSError) {
        self.completion?(feed: nil, error: parseError)
    }
    
    func parser(parser: NSXMLParser, validationErrorOccurred validationError: NSError) {
        self.completion?(feed: nil, error: validationError)
    }
    
    // MARK: - Helpers
    
    private func parseXMLData(data: NSData) {
        self.parser = NSXMLParser(data: data)
        if let parser = self.parser {
            parser.delegate = self
            parser.shouldResolveExternalEntities = false
            parser.parse()
        } else {
            let error = self.errorWithHost(
                self.feed.rssLink,
                message: L10N(UnableToParseRSSFeedByURL) + ":/n" + self.feed.rssLink + "/n" + L10N(CheckTheSourceAndTryAgain)
            )
            self.completion?(feed: nil, error: error)
        }
    }
    
    private func errorWithHost(host: String?, message: String) -> NSError {
        return NSError(
            domain: host ?? "",
            code: 0,
            userInfo: [NSLocalizedDescriptionKey : message]
        )
    }
    
    struct DocumentsConstants {
        static let DocumentsDirectoryPath: AnyObject = NSSearchPathForDirectoriesInDomains(.DocumentDirectory,.UserDomainMask,true)[0]
    }
    
    private func saveFeedToFile(data: NSData) {
        // Save rss data to disk
        let path = (DocumentsConstants.DocumentsDirectoryPath as! String) + "/\(self.feed.rssLink.hash)"
        data.writeToFile(path, atomically: true)
    }
    
    private func loadFeedFromFile() -> NSData? {
        let path = (DocumentsConstants.DocumentsDirectoryPath as! String) + "/\(self.feed.rssLink.hash)"
        return NSData(contentsOfFile: path)
    }
}