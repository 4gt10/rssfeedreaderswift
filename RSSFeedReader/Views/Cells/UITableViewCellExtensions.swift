//
//  UITableViewCellExtensions.swift
//  RSSReader
//
//  Created by 4gt10 on 19.01.16.
//  Copyright © 2016 Artur Chernov. All rights reserved.
//

import UIKit

extension UITableViewCell {
    
    class func cellIdentifier() -> String {
        return NSStringFromClass(self)
    }
}