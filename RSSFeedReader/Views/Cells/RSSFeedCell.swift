//
//  RSSFeedCell.swift
//  RSSReader
//
//  Created by 4gt10 on 19.01.16.
//  Copyright © 2016 Artur Chernov. All rights reserved.
//

import UIKit

class RSSFeedCell: UITableViewCell {
    
    @IBOutlet weak var feedItemTitleLabel: UILabel!
    @IBOutlet weak var feedItemDescriptionLabel: UILabel!
    @IBOutlet weak var feedItemDateLabel: UILabel!
}
